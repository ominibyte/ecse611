package ca.mcgill.ecse611.group2.replication;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.me.JSONObject;

import ca.mcgill.ecse611.ServerPostingInterface;
import ca.mcgill.ecse611.ServerPostingManager;
import ca.mcgill.ecse611.Util;
import ca.mcgill.ecse611.Util.ErrorContext;
import ca.mcgill.ecse611.Util.SimpleMap;

public class PyPiProcessor{
	public static final String PYPI_SIMPLE_URL = "https://pypi.python.org/simple/";
	public static final String PYPI_PYPI_URL = "https://pypi.python.org/pypi/";
	private LinkedList<String> projectNames;
	private ArrayBlockingQueue<SimpleMap<String, String>> urlQueue;
	private ExecutorService executor;
	private boolean producerFinished;
	private ArrayList<SimpleMap<String, String>> projectURLMapList;
	private static final String OUTPUT_FILE_PATH = "C:\\Users\\Richboy\\Documents\\ECSE611\\project_url_map.csv";
	private static final String FAILED_FILE_PATH = "C:\\Users\\Richboy\\Documents\\ECSE611\\failed_url_map.csv";
	private static final String JSON_DIRECTORY_PATH = "C:\\Users\\Richboy\\Documents\\ECSE611\\json_files\\";
	private ArrayList<SimpleMap<String, String>> failedURLs;
	
	public PyPiProcessor(){
		projectNames = new LinkedList<>();
		urlQueue = new ArrayBlockingQueue<>(10);
		executor = Executors.newFixedThreadPool(4);
		projectURLMapList = new ArrayList<>();
		failedURLs = new ArrayList<>();
		
		process();
	}
	
	private void process(){
		fetchProjectNames();
		
		System.out.println(projectNames.size() + " PyPI Projects found!");
		
		filterForGitHubProjects();
		System.out.println(failedURLs.size() + " PyPI Projects failed access.");
		System.out.println(projectURLMapList.size() + " PyPI Projects left after filtering.");
		
		generateCSVFile();
		generateFailedCSVFile();
		
		executor.shutdown();
		System.out.println("\n\nDone!!!");
	}
	
	private void fetchProjectNames(){
		System.out.println("Getting PyPI Projects...");
		
		//get the project names from the simple url
		ServerPostingManager spm = new ServerPostingManager(new ServerPostingInterface() {
			
			@Override
			public void retriveMessage(ErrorContext context) {
				if( context.isSuccessful() ){
					filterForURLs(context.getMessage());
				}
				else{
					System.err.println(context.getMessage());
				}
			}
			
			@Override
			public void interruptPosting(String text) {
				System.err.println(text);
			}
		});
		spm.setTargetURL(PYPI_SIMPLE_URL);
		spm.setAsGetRequest();
		spm.setURLParameters("");
		spm.run();
	}
	
	private void filterForURLs(String content){
		Pattern p = Pattern.compile("href='\\S+'");
		Matcher m = p.matcher(content);
		
		
		String text;
		for( ; m.find(); ){
			text = m.group();
			projectNames.add(text.substring(text.indexOf("'") + 1, text.lastIndexOf("'")));
		}
	}
	
	private void filterForGitHubProjects(){
		System.out.println("\nApplying filter for non GitHub projects...");
		
		executor.execute(new Producer());
		Future<ArrayList<SimpleMap<String, String>>>[] futures = new Future[3];
		
		for(int i = 0; i < futures.length; i++){
			futures[i] = executor.submit(new Consumer());
		}
		
		for(int i = 0; i < futures.length; i++){
			try{
				projectURLMapList.addAll(futures[i].get());
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	private void generateCSVFile(){
		System.out.println("\nGenerating report to CSV file...");
		
		File csvFile = new File(OUTPUT_FILE_PATH);
		csvFile.delete();
		
		//add CSV file header
		projectURLMapList.add(0, new SimpleMap<String, String>("Project", "GitHub URL"));
		
		try( BufferedWriter writer = new BufferedWriter(new FileWriter(csvFile)) ){
			projectURLMapList.forEach(pair -> {
				try{
					writer.append(pair.getFirstElement() + "," + pair.getSecondElement());
					writer.newLine();
				}catch(Exception e){ e.printStackTrace(); }
			});
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void generateFailedCSVFile(){
		System.out.println("\nGenerating Failed report to CSV file...");
		
		File csvFile = new File(FAILED_FILE_PATH);
		csvFile.delete();
		
		//add CSV file header
		failedURLs.add(0, new SimpleMap<String, String>("Project", "PyPI URL"));
		
		try( BufferedWriter writer = new BufferedWriter(new FileWriter(csvFile)) ){
			failedURLs.forEach(pair -> {
				try{
					writer.append(pair.getFirstElement() + "," + pair.getSecondElement());
					writer.newLine();
				}catch(Exception e){ e.printStackTrace(); }
			});
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new PyPiProcessor();
	}
	
	private class Producer implements Runnable{

		@Override
		public void run() {
			projectNames.forEach(project -> {
				try{
					urlQueue.put(new SimpleMap<>(project, PYPI_PYPI_URL + project + "/json"));
				}catch(Exception e){
					e.printStackTrace();
				}
			});
			producerFinished = true;
		}
		
	}

	private class Consumer implements Callable<ArrayList<SimpleMap<String, String>>>{
		private ArrayList<SimpleMap<String, String>> pairList;
		
		public Consumer(){
			pairList = new ArrayList<>();
		}
		
		public ArrayList<SimpleMap<String, String>> call(){
			do{
				try{
					SimpleMap<String, String> temp = urlQueue.poll();
					
					while( temp == null && !producerFinished ){
						Thread.sleep(100);
						temp = urlQueue.poll();
					}
					
					if( temp == null )//no more data
						break;
					
					final SimpleMap<String, String> urlMap = temp;
					
					ServerPostingManager spm = new ServerPostingManager(new ServerPostingInterface() {
						
						@Override
						public void retriveMessage(ErrorContext context) {
							if( context.isSuccessful() ){
								try{
									JSONObject obj = new JSONObject(context.getMessage());
									String homePage = obj.getJSONObject("info").getString("home_page");
									
									if( !homePage.toLowerCase().contains("github.com") ){
										pairList.add(new SimpleMap<>(urlMap.getFirstElement(), homePage));
										
										Files.write(new File(JSON_DIRECTORY_PATH, urlMap.getFirstElement() + "-" +  Util.generateUniqueID() + ".json").toPath(), context.getMessage().getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
									}
								}
								catch(Exception e){
									e.printStackTrace();
								}
							}
							else{
								synchronized(failedURLs){
									failedURLs.add(urlMap);
								}
								//System.err.println(context.getMessage());
							}
						}
						
						@Override
						public void interruptPosting(String text) {
							System.err.println(text);
						}
					});
					spm.setTargetURL(urlMap.getSecondElement());
					spm.setAsGetRequest();
					spm.setURLParameters("");
					spm.run();
				}
				catch(InterruptedException e){
					e.printStackTrace();
				}
			}while(true);
			
			return pairList;
		}
	}
}
