package ca.mcgill.ecse611.group2.replication;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import org.json.me.JSONObject;

import ca.mcgill.ecse611.Util.SimpleMap;

public class Graph11DataGenerator {
	private Map<String, Integer> versionCountMap;
	private Map<String, ArrayList<Double>> versionAgeMap;
	private static final String JSON_FILES_FOLDER = "/Users/Richboy/Documents/ECSE611/Replication/json_files";
	private static final String CSV_FILE1 = "/Users/Richboy/Documents/ECSE611/Replication/graph11_major_data.csv";
	private static final String CSV_FILE2 = "/Users/Richboy/Documents/ECSE611/Replication/graph11_minor_data.csv";
	private static final String CSV_FILE3 = "/Users/Richboy/Documents/ECSE611/Replication/graph11_patch_data.csv";
	
	public Graph11DataGenerator(){
		versionCountMap = new HashMap<>();
		versionAgeMap = new HashMap<>();
		
		gatherData();
		filterData();
		cleanup();
	}
	
	//gather all the version data
	private void gatherData(){
		//get the unique versions by searching for all packages
		File folder = new File(JSON_FILES_FOLDER);
		
		File[] files = folder.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().trim().endsWith(".json");
			}
		});
		
		JSONObject obj, releases;
		Enumeration<String> versions;
		String version, firstVersion;
		int count = 0;
		int total = files.length;
		ArrayList<String> versionsList = new ArrayList<>();
		ArrayList<Double> ages;
		
		System.out.println(total + " files found!\n");
		
		for(File file : files){
			count++;
			
			System.out.printf("\r%d of %d (%d%s)", count, total, count * 100 / total, "%");
			
			try{
				obj = new JSONObject(new String(Files.readAllBytes(file.toPath())));
				releases = obj.getJSONObject("releases");
				versions = releases.keys();
				firstVersion = "1000.1000.1000";
				versionsList.clear();
				
				while( versions.hasMoreElements() ){
					try{
						version = versions.nextElement();
						if( isSmaller(version, firstVersion) )
							firstVersion = version;
						
						if( versionCountMap.containsKey(regularizeVersion(version)) )
							versionCountMap.put(regularizeVersion(version), versionCountMap.get(regularizeVersion(version)) + 1);
						else
							versionCountMap.put(regularizeVersion(version), 1);
						
						versionsList.add(version);
					}
					catch(Exception e){
					}
				}
				
				for(String v : versionsList){
					double age = getVersionAgeInMonths(getUploadTime(releases, firstVersion), getUploadTime(releases, v));
					if( versionAgeMap.containsKey(regularizeVersion(v)) )
						ages = versionAgeMap.get(regularizeVersion(v));
					else
						ages = new ArrayList<>();
					
					ages.add(age);
					versionAgeMap.put(regularizeVersion(v), ages);
				}
			}
			catch(Exception e){
				System.err.println(file + " - " + e.getMessage());
				//e.printStackTrace();
			}
		}
	}
	
	//filter for only the versions that are up to 100
	private void filterData(){
		Iterator<Map.Entry<String, Integer>> iterator = versionCountMap.entrySet().iterator();
		Map.Entry<String, Integer> entry;
		ArrayList<Double> ages;
		
		LinkedList<SimpleMap<String, Double>> major = new LinkedList<>(), 
				minor = new LinkedList<>(), 
				patch = new LinkedList<>(),
				group;
		
		try( BufferedWriter writer1 = new BufferedWriter(new FileWriter(new File(CSV_FILE1))); 
				BufferedWriter writer2 = new BufferedWriter(new FileWriter(new File(CSV_FILE2)));
				BufferedWriter writer3 = new BufferedWriter(new FileWriter(new File(CSV_FILE3))) ){
			writer1.write("Major Version,Version Weight,Average Age");
			writer1.newLine();
			
			writer2.write("Minor Version,Version Weight,Average Age");
			writer2.newLine();
			
			writer3.write("Patch Version,Version Weight,Average Age");
			writer3.newLine();
			
			while(iterator.hasNext()){
				entry = iterator.next();
				if( entry.getValue() >= 100 ){
					ages = versionAgeMap.get(entry.getKey());
					
					if( isPatchVersion(entry.getKey()) )
						group = patch;
					else if( isMinorVersion(entry.getKey()) )
						group = minor;
					else
						group = major;
					
					
					group.add(new SimpleMap<>(entry.getKey(), (ages.stream().mapToDouble(n -> n).average().getAsDouble() * 3.80265176 * Math.pow(10, -10))));
				}
			}
			
			Collections.sort(major, new VersionComparator());
			Collections.sort(minor, new VersionComparator());
			Collections.sort(patch, new VersionComparator());
			
			for(SimpleMap<String, Double> pair : major){
				writer1.write(pair.getFirstElement() + "," + calculateVersionValue(pair.getFirstElement()) + "," + pair.getSecondElement());
				writer1.newLine();
			}
			
			for(SimpleMap<String, Double> pair : minor){
				writer2.write(pair.getFirstElement() + "," + calculateVersionValue(pair.getFirstElement()) + "," + pair.getSecondElement());
				writer2.newLine();
			}
			
			for(SimpleMap<String, Double> pair : patch){
				writer3.write(pair.getFirstElement() + "," + calculateVersionValue(pair.getFirstElement()) + "," + pair.getSecondElement());
				writer3.newLine();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void cleanup(){
		versionAgeMap.clear();
		versionCountMap.clear();
		
		System.out.println("\n\nDONE!!!");
	}
	
	private boolean isSmaller(String version1, String version2) throws Exception{
		String[] parts1 = version1.split("\\.");
		String[] parts2 = version2.split("\\.");
		
		int major1 = Integer.parseInt(parts1[0]);
		int minor1 = parts1.length > 1 ? Integer.parseInt(parts1[1]) : 0;
		int patch1 = parts1.length > 2 ? Integer.parseInt(parts1[2]) : 0;
		
		int major2 = Integer.parseInt(parts2[0]);
		int minor2 = parts2.length > 1 ? Integer.parseInt(parts2[1]) : 0;
		int patch2 = parts2.length > 2 ? Integer.parseInt(parts2[2]) : 0;
		
		if( major1 < major2 )
			return true;
		if( major1 == major2 && minor1 < minor2 )
			return true;
		if( major1 == major2 && minor1 == minor2 && patch1 < patch2 )
			return true;
		
		return false;
	}
	
	//referenceDate is the date of the first version
	private double getVersionAgeInMonths(String referenceDate, String versionDate) throws Exception{
		double age = 0;
		//2014-12-28T00:54:02
		long referenceTS = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(referenceDate).getTime();
		long versionTS = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(versionDate).getTime();
		
		age = versionTS - referenceTS;
		
		return Math.abs(age);
	}
	
	private boolean isPatchVersion(String version){
		String[] parts = version.split("\\.");
		
		return (parts[2].trim().length() == 1 && !version.endsWith("0")) || (parts[2].trim().length() > 1);
	}
	
	private boolean isMinorVersion(String version){
		String[] parts = version.split("\\.");
		
		return !isPatchVersion(version) && 
				((parts[1].trim().length() == 1 && !parts[1].endsWith("0")) || (parts[1].trim().length() > 1));
	}
	
	private boolean isMajorVersion(String version){
		return !isPatchVersion(version) && !isMinorVersion(version);
	}
	
	private String getUploadTime(JSONObject releases, String version) throws Exception{
		return releases.getJSONArray(version).getJSONObject(0).getString("upload_time");
	}
	
	private String regularizeVersion(String v){
//		String v = version + "";
//		while( v.split("\\.").length < 3 )
//			v += ".0";
//		
//		try{
//			String[] parts = v.split("\\.");
//			
//			v = Integer.parseInt(parts[0]) + "." + Integer.parseInt(parts[1]) + "." + Integer.parseInt(parts[2]);
//		}catch(Exception e){}
//		
//		return v;
		
		String version = v + "";
		version = version.replaceAll("[a-zA-Z]", ".");
		
		while( version.contains("..") )
			version = version.replace("..", ".");
		
		while( version.split("\\.").length < 3 )
			version += ".0";
		
		while( version.contains("..") )
			version = version.replace("..", ".");
		
		try{
			String[] parts = version.split("\\.");
			
			version = Integer.parseInt(parts[0]) + "." + Integer.parseInt(parts[1]) + "." + Integer.parseInt(parts[2]);
		}catch(Exception e){}
		
		return version;
	}
	
	private double calculateVersionValue(String version){
		String[] parts = version.split("\\.");
		
		return Integer.parseInt(parts[0]) + function(Integer.parseInt(parts[1]) + function(Integer.parseInt(parts[2])));
	}
	
	private double function(double number){
		return 1 - (1 / (0.5 * number + 1));
	}
	
	public static void main(String[] args){
		new Graph11DataGenerator();
	}
	
	private class VersionComparator implements Comparator<SimpleMap<String, Double>>{

		@Override
		public int compare(SimpleMap<String, Double> o1,
				SimpleMap<String, Double> o2) {
			try{
				if( isSmaller(o1.getFirstElement(), o2.getFirstElement()) )
					return -1;
				else if( isSmaller(o2.getFirstElement(), o1.getFirstElement()) )
					return 1;
				else
					return 0;
			}
			catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}
}
