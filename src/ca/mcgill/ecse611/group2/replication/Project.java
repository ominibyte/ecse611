package ca.mcgill.ecse611.group2.replication;

import java.util.HashMap;
import java.util.Map;

public class Project {
	private int id;
	private String url;
	private boolean deleted;
	private String createdAt;
	private String updatedAt;
	private int forkedFrom;
	private Map<String, Object> metaDataMap;
	
	public Project(int id, String url, boolean deleted, String createdAt, String updatedAt, int forkedFrom){
		this.id = id;
		this.url = url;
		this.deleted = deleted;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.forkedFrom = forkedFrom;
		metaDataMap = new HashMap<>();
	}

	public int getId() {
		return id;
	}

	public String getUrl() {
		return url;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public int getForkedFrom() {
		return forkedFrom;
	}
	
	public void addMetaData(String key, Object value){
		metaDataMap.put(key, value);
	}
	
	public Object getMetaData(String key){
		return metaDataMap.get(key);
	}
}
