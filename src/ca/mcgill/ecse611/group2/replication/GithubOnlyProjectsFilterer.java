package ca.mcgill.ecse611.group2.replication;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import ca.mcgill.ecse611.Query;

public class GithubOnlyProjectsFilterer {
	private LinkedList<String> pyPiProjects;
	private static final String PYPI_PROJECTS_CSV = "/Users/Richboy/Documents/ECSE611/Replication/project_url_map.csv";
	private ExecutorService executor;
	
	public GithubOnlyProjectsFilterer(){
		pyPiProjects = new LinkedList<>();
		executor = Executors.newFixedThreadPool(4);
		
		loadPyPiProjects();
		filterForGithubOnlyProjects();
		printGithubOnlyProjectsCount();
		
		executor.shutdown();
		System.out.println("\n\nDone!!!");
	}
	
	private void loadPyPiProjects(){
		File file = new File(PYPI_PROJECTS_CSV);
		
		int row = 0;
		String line;
		String[] parts;
		
		try(BufferedReader reader = new BufferedReader(new FileReader(file))){
			while( (line = reader.readLine()) != null ){
				row++;
				if( row == 1 )
					continue;
				
				parts = line.split(",");
				
				pyPiProjects.add("https://api.github.com/repos/" + parts[1].substring(parts[1].indexOf("com/") + "com/".length()));
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void filterForGithubOnlyProjects(){
		int count = 0;
		
		//get the total number of projects in the database
		Query q = new Query("SELECT COUNT(*) AS cnt FROM python_projects");
		if( q.isFullyConnected() ){
			try{
				ResultSet rs = q.getPS().executeQuery();
				rs.next();
				count = rs.getInt("cnt");
				rs.close();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		else{
			q.disconnect();
			q.getException().printStackTrace();
			return;
		}
		q.disconnect();
		
		System.out.println("Total Count is " + count);
		
		Future<String>[] futures = new Future[5];
		
		int batchCount = count / futures.length;
		
		for(int i = 0; i < futures.length; i++){
			if( i == futures.length - 1 )//last item
				futures[i] = executor.submit(new Runner(i * batchCount, batchCount + (count - batchCount * futures.length)), "");
			else
				futures[i] = executor.submit(new Runner(i * batchCount, batchCount), "");
		}
		
		for(int i = 0; i < futures.length; i++){
			try {
				futures[i].get();
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void printGithubOnlyProjectsCount(){
		//get the total number of projects in the database
		Query q = new Query("SELECT COUNT(*) AS cnt FROM github_only_projects");
		if( q.isFullyConnected() ){
			try{
				ResultSet rs = q.getPS().executeQuery();
				rs.next();
				System.out.println(rs.getInt("cnt") + " Projects exists in Github only!");
				rs.close();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		else{
			q.disconnect();
			q.getException().printStackTrace();
			return;
		}
		q.disconnect();
	}
	
	public static void main(String[] args){
		new GithubOnlyProjectsFilterer();
	}
	
	private class Runner implements Runnable{
		private int startIndex;
		private int endIndex;
		private Query query;
		
		public Runner(int startIndex, int length){
			this.startIndex = startIndex;
			this.endIndex = length + startIndex;
			
			System.out.println("From " + startIndex + " to " + endIndex);
		}
		
		public void run(){
			Query q = new Query("SELECT * FROM python_projects ORDER BY id OFFSET ? LIMIT 1");
			if( q.isFullyConnected() ){
				try{
					PreparedStatement ps = q.getPS();
					ResultSet rs;
					for(int i = startIndex; i < endIndex; i++){
						ps.setInt(1, i);
						
						rs = ps.executeQuery();
						rs.next();
						
						Project p = new Project(rs.getInt("id"), rs.getString("url"), rs.getBoolean("deleted"), 
								rs.getString("created_at"), rs.getString("created_at"), rs.getInt("forked_from"));
						
						rs.close();
						
						if( !pyPiProjects.contains(p.getUrl()) )
							saveProject(p);
					}
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			else{
				q.getException().printStackTrace();
			}
			q.disconnect();
			
			if( query != null )
				query.disconnect();
		}
		private void saveProject(Project project){
			if( query == null ){
				query = new Query("INSERT INTO github_only_projects VALUES (?, ?, ?, ?, ?, ?)");
				if( !query.isFullyConnected() ){
					query.getException().printStackTrace();
					query = null;
					return;
				}
			}
			
			try{
				PreparedStatement ps = query.getPS();
				
				ps.setInt(1, project.getId());
				ps.setString(2, project.getUrl());
				ps.setBoolean(3, project.isDeleted());
				ps.setString(4, project.getCreatedAt());
				ps.setString(5, project.getUpdatedAt());
				ps.setInt(6, project.getForkedFrom());
				
				ps.executeUpdate();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}
}
