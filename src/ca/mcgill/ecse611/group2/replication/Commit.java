package ca.mcgill.ecse611.group2.replication;

public class Commit {
	private long id;
	private String sha;
	private int authorID;
	private int commiterID;
	private int projectID;
	private String createdAt;
	
	public Commit(long id, String sha, int authorID, int commiterID,
			int projectID, String createdAt) {
		this.id = id;
		this.sha = sha;
		this.authorID = authorID;
		this.commiterID = commiterID;
		this.projectID = projectID;
		this.createdAt = createdAt;
	}

	public long getId() {
		return id;
	}

	public String getSha() {
		return sha;
	}

	public int getAuthorID() {
		return authorID;
	}

	public int getCommiterID() {
		return commiterID;
	}

	public int getProjectID() {
		return projectID;
	}

	public String getCreatedAt() {
		return createdAt;
	}
}
