package ca.mcgill.ecse611.group2.replication;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import ca.mcgill.ecse611.DownloadListener;
import ca.mcgill.ecse611.Query;
import ca.mcgill.ecse611.ServerPostingInterface;
import ca.mcgill.ecse611.ServerPostingManager;
import ca.mcgill.ecse611.Util;
import ca.mcgill.ecse611.Util.ErrorContext;
import ca.mcgill.ecse611.Util.SimpleMap;

public class DependencyExtractor{
	private ExecutorService executor;
	private ArrayBlockingQueue<Project> wholeSalerQueue;
	private ArrayBlockingQueue<SimpleMap<Project, File>> retailerQueue;
	private ArrayBlockingQueue<SimpleMap<Project, Set<String>>> consumerQueue;
	private boolean producerFinished, producerStarted;
	private int retailerCount, wholeSalerCount;
	private static final int THREAD_COUNT = 7;
	private static File tempFolder;
	private static final String FAILED_DOWNLOADS_PATH = "C:\\Users\\Richboy\\Documents\\ECSE611\\failed_downloads.csv";
	private static final String FAILED_DEPENDENCIES_PATH = "C:\\Users\\Richboy\\Documents\\ECSE611\\failed_dependencies.csv";
	private static final String TEMP_FOLDER_PATH = "C:\\Users\\Richboy\\Documents\\ECSE611\\temp\\";
	private Set<Integer> alreadyProcessed;
	private BufferedWriter downloadsWriter, dependenciesWriter;
	
	public DependencyExtractor(){
		executor = Executors.newFixedThreadPool(THREAD_COUNT);
		wholeSalerQueue = new ArrayBlockingQueue<>(10);
		retailerQueue = new ArrayBlockingQueue<>(10);
		consumerQueue = new ArrayBlockingQueue<>(10);
		alreadyProcessed = new HashSet<>();
		
		try {
			downloadsWriter = new BufferedWriter(new FileWriter(FAILED_DOWNLOADS_PATH));
			dependenciesWriter = new BufferedWriter(new FileWriter(FAILED_DEPENDENCIES_PATH));
			tempFolder = Files.createTempDirectory("DepEx").toFile();
			ServerPostingManager.TEMP_DOWNLOAD_DIRECTORY = tempFolder;
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		init();
		
		//loadAlreadyProcessed();
		startProcessing();
		
		finalizeCSVFiles();
		
		//do some house keeping...
		Util.deleteFileAndDirectory(tempFolder);
		
		executor.shutdown();
		System.out.println("\n\nDone!!!");
	}
	
	private void init(){
		try{
			File csvFile = new File(FAILED_DOWNLOADS_PATH);
			csvFile.delete();
			csvFile.createNewFile();
			
			downloadsWriter.write("Project ID, Project URL, Download URL");
			downloadsWriter.newLine();
			
			csvFile = new File(FAILED_DEPENDENCIES_PATH);
			csvFile.delete();
			csvFile.createNewFile();
			
			dependenciesWriter.write("Project ID, Project URL, Dependency");
			dependenciesWriter.newLine();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void loadAlreadyProcessed(){
		Query q = new Query("SELECT id FROM github_final_projects", Util.mcon);
		if( q.isFullyConnected() ){
			try{
				ResultSet rs = q.getPS().executeQuery();
				while(rs.next()){
					alreadyProcessed.add(rs.getInt("id"));
				}
				rs.close();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		else{
			q.getException().printStackTrace();
		}
		q.disconnect();
	}
	
	private void startProcessing(){
		//1 producer
		//2 wholesaler
		//2 retailers
		//2 consumers
		
		List<Future<String>> futures = new ArrayList<>();
		
		executor.execute(new Producer());
		
		for(int i = 0; i < 2; i++){
			futures.add(executor.submit(new WholeSaler(), ""));
		}
		
		try{
			Thread.sleep(200);
		}catch(Exception e){}
		
		for(int i = 0; i < 2; i++){
			futures.add(executor.submit(new Retailer(), ""));
		}
		
		try{
			Thread.sleep(200);
		}catch(Exception e){}
		
		for(int i = 0; i < 2; i++){
			futures.add(executor.submit(new Consumer(), ""));
		}
		
		for(Future<String> future : futures){
			try {
				future.get();
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void finalizeCSVFiles(){
		try{
			downloadsWriter.close();
			dependenciesWriter.close();
		}catch(Exception e){}
	}
	
	public static void main(String[] args){
		new DependencyExtractor();
	}
	
	public class Producer implements Runnable{
		private int total;
		
		public void run(){
			System.out.println("Starting Producer...");
			
			Query q = new Query("SELECT * FROM github_only_projects WHERE deleted = 0 AND id > 1328447 ORDER BY id", Util.mcon);
			if( q.isFullyConnected() ){
				try{
					ResultSet rs = q.getPS().executeQuery();
					
					producerStarted = true;
					
					rs.last();
					total = rs.getRow();
					int count = 0;
					rs.beforeFirst();
					
					while(rs.next()){
						try{
							System.out.println(count++ + " of " + total);
							
//							if( alreadyProcessed.contains(rs.getInt("id")) )
//								continue;
							
							Project p = new Project(rs.getInt("id"), rs.getString("url"), rs.getBoolean("deleted"), 
									rs.getString("created_at"), rs.getString("created_at"), rs.getInt("forked_from"));
							
							//check the queue is already full
							while( wholeSalerQueue.remainingCapacity() == 0 ){//if queue is full
								//check for works elsewhere
								//wholesale, retailer and then consumer
								
//								if( (!wholeSalerQueue.isEmpty() && wholeSalerQueue.remainingCapacity() > 0) || retailerQueue.isEmpty() )
//									new WholeSaler(1).run();
//								else if( (!retailerQueue.isEmpty() && retailerQueue.remainingCapacity() > 0) || consumerQueue.isEmpty() )
//									new Retailer(1).run();
//								else if( !consumerQueue.isEmpty() && consumerQueue.remainingCapacity() > 0 )
//									new Consumer(1).run();
								
								if( !consumerQueue.isEmpty() && consumerQueue.remainingCapacity() >= 0 && consumerQueue.remainingCapacity() < 3 )
									new Consumer(1).run();
								else if( (!retailerQueue.isEmpty() && retailerQueue.remainingCapacity() >= 0 && retailerQueue.remainingCapacity() < 4) || consumerQueue.isEmpty() )
									new Retailer(1).run();
								else if( (!wholeSalerQueue.isEmpty() && wholeSalerQueue.remainingCapacity() >= 0 && wholeSalerQueue.remainingCapacity() < 5) || retailerQueue.isEmpty() )
									new WholeSaler(1).run();
							}
							
							wholeSalerQueue.put(p);
						}
						catch(Exception e){
						}
					}
					rs.close();
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			else{
				q.getException().printStackTrace();
			}
			q.disconnect();
			
			producerFinished = true;
		}
	}
	
	private List<SimpleMap<Project, File>> wsHelpersList = new ArrayList<>();
	private Integer whCount = 0;
	
	private class WholeSaler implements Runnable, DownloadListener{
		private Project currentProject;
		private int runTimes;
		private List<SimpleMap<Project, File>> helpersListCopy;
		private int whNumber;
		
		public WholeSaler(){
			wholeSalerCount++;
			helpersListCopy = new ArrayList<>();
			synchronized(whCount){
				if( whCount < Integer.MAX_VALUE )
					whNumber = whCount++;
				else
					whNumber = whCount = 2; 
			}
		}
		
		public WholeSaler(int runTimes){
			this();
			this.runTimes = runTimes;
		}
		
		public void run(){
			System.out.println("Starting WholeSaler..." + whNumber);
			
			while( !producerStarted ){
				try{
					Thread.sleep(100);
				}catch(Exception e){}
			}
			
			do{
				try{
					if( runTimes == 0 && !wsHelpersList.isEmpty() ){
						helpersListCopy.clear();
						
						synchronized(wsHelpersList){
							helpersListCopy.addAll(wsHelpersList);
							wsHelpersList.clear();
						}
						
						for(SimpleMap<Project, File> entry : helpersListCopy){
							try{
								retailerQueue.put(entry);
							}catch(Exception e){}
						}
						helpersListCopy.clear();
					}
					
					Project p = wholeSalerQueue.poll();
					
					while( p == null && !producerFinished ){
						if( runTimes == 1 )
							break;
						
						//check if there is work to be done elsewhere
						//retailer first, then consumer
//						if( (!retailerQueue.isEmpty() && retailerQueue.remainingCapacity() > 0) || consumerQueue.isEmpty() )
//							new Retailer(1).run();
//						else if( !consumerQueue.isEmpty() && consumerQueue.remainingCapacity() > 0 )
//							new Consumer(1).run();
						
						if( !consumerQueue.isEmpty() && consumerQueue.remainingCapacity() >= 0 && consumerQueue.remainingCapacity() < 3 )
							new Consumer(1).run();
						else if( (!retailerQueue.isEmpty() && retailerQueue.remainingCapacity() >= 0 && retailerQueue.remainingCapacity() < 4) || consumerQueue.isEmpty() )
							new Retailer(1).run();
						
						p = wholeSalerQueue.poll();
					}
					
					if( p == null )
						break;
					
					currentProject = p;
					
					downloadProject(p);
					
					if( runTimes == 1 )
						break;
				}
				catch(Exception e){}
			}while(true);
			
			wholeSalerCount--;
			
			System.out.println("Ending WholeSaler..." + whNumber);
		}
		
		private String buildDownloadURL(Project project){
			String url = currentProject.getUrl().trim();
			
			return url + (url.endsWith("/") ? "" : "/") + "zipball/master";
			//return "https://github.com:443/" + url.substring(url.indexOf("/repos/") + "/repos/".length()) + (url.endsWith("/") ? "" : "/") + "archive/master.zip";
		}
		
		private void downloadProject(Project project){
			ServerPostingManager spm = new ServerPostingManager(null);
			spm.setAsDownload(this);
			spm.setTargetURL(buildDownloadURL(project));
			spm.setURLParameters("");
			spm.setAsGetRequest();
			spm.setRetriesCount(3);
			spm.setShouldGenerateRandomDownloadFileName();
			
			System.out.println("Starting Download for WH-" + whNumber);
			
			spm.run();
		}

		@Override
		public void onFailure(String url) {
			try{
				downloadsWriter.write(currentProject.getId() + "," + currentProject.getUrl() + "," + url);
				downloadsWriter.newLine();
			}catch(Exception e){}
			
			System.err.println("Failed to download: " + url);
			System.out.println("Finished/Failed Download for WH-" + whNumber);
		}

		@Override
		public void onFinish(long downloaded, String url, File savedFilePath) {
			System.out.println("Finished/Success Download for WH-" + whNumber);
			try {
				if( runTimes == 1 ){
					boolean state = retailerQueue.offer(new SimpleMap<>(currentProject, savedFilePath));
					if( !state )
						wsHelpersList.add(new SimpleMap<>(currentProject, savedFilePath));
				}
				else
					retailerQueue.put(new SimpleMap<>(currentProject, savedFilePath));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onStart(long total) {
		}

		@Override
		public void publish(long downloaded) {
		}

		@Override
		public void onCancel(long downloaded, String url, File savedFilePath) {
		}
	}
	
	private List<SimpleMap<Project, Set<String>>> rHelpersList = new ArrayList<>();
	private Integer rCount = 0;
	
	private class Retailer implements Runnable{
		private int runTimes;
		private List<SimpleMap<Project, Set<String>>> helpersListCopy;
		private int rNumber;
		
		public Retailer(){
			retailerCount++;
			helpersListCopy = new ArrayList<>();
			synchronized(rCount){
				if( rCount < Integer.MAX_VALUE )
					rNumber = rCount++;
				else
					rNumber = rCount = 2;
			}
		}
		
		public Retailer(int runTimes){
			this();
			this.runTimes = runTimes;
		}
		
		public void run(){
			System.out.println("Starting Retailer..." + rNumber);
			
			while( !producerStarted ){
				try{
					Thread.sleep(100);
				}catch(Exception e){}
			}
			
			do{
				try{
					if( runTimes == 0 && !rHelpersList.isEmpty() ){
						helpersListCopy.clear();
						
						synchronized(rHelpersList){
							helpersListCopy.addAll(rHelpersList);
							rHelpersList.clear();
						}
						
						for(SimpleMap<Project, Set<String>> entry : helpersListCopy){
							try{
								consumerQueue.put(entry);
							}catch(Exception e){}
						}
						helpersListCopy.clear();
					}
					
					SimpleMap<Project, File> projectFileMap = retailerQueue.poll();
					
					while( projectFileMap == null && wholeSalerCount > 0 ){
						if( runTimes == 1 )
							break;
						
						//check if some other work exists somewhere to be done
						//wholesaler first, then consumer
//						if( (!wholeSalerQueue.isEmpty() && wholeSalerQueue.remainingCapacity() > 0) || retailerQueue.isEmpty() )
//							new WholeSaler(1).run();
//						else if( !consumerQueue.isEmpty() && consumerQueue.remainingCapacity() > 0 )
//							new Consumer(1).run();
						
						if( !consumerQueue.isEmpty() && consumerQueue.remainingCapacity() >= 0 && consumerQueue.remainingCapacity() < 3 )
							new Consumer(1).run();
						else if( (!wholeSalerQueue.isEmpty() && wholeSalerQueue.remainingCapacity() >= 0 && wholeSalerQueue.remainingCapacity() < 5) || retailerQueue.isEmpty() )
							new WholeSaler(1).run();
						
						projectFileMap = retailerQueue.poll();
					}
					
					if( projectFileMap == null )
						break;
					
					System.out.println("Retailer..." + rNumber + " Starting Processing");
					
					File directory = new File(TEMP_FOLDER_PATH, Util.generateUniqueID());
					directory.getParentFile().mkdirs();
					
					Util.extractZipTo(projectFileMap.getSecondElement(), directory, true);
					
					boolean hasSetupFile = hasFile(directory, "setup.py");
					boolean hasRequirementsFile = hasFile(directory, "requirements.txt");
					
					projectFileMap.getFirstElement().addMetaData("hasSetupFile", hasSetupFile);
					projectFileMap.getFirstElement().addMetaData("hasRequirementsFile", hasRequirementsFile);
					
					Set<String> dependencies = extractDependencies(directory);
					
					if( dependencies.size() > 0 ){
						try {
							if( runTimes == 0 )
								consumerQueue.put(new SimpleMap<>(projectFileMap.getFirstElement(), dependencies));
							else{
								boolean state = consumerQueue.offer(new SimpleMap<>(projectFileMap.getFirstElement(), dependencies));
								if( !state )
									rHelpersList.add(new SimpleMap<>(projectFileMap.getFirstElement(), dependencies));
							}
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					else{
						System.out.println("No dependency");
					}
					
					//perform housekeeping tasks...delete the extracted directory and zip file
					if( projectFileMap.getSecondElement().exists() )
						projectFileMap.getSecondElement().delete();
					Util.deleteFileAndDirectory(directory);
					
					System.out.println("Retailer..." + rNumber + " Ending Processing");
					
					if( runTimes == 1 )
						break;
				}
				catch(Exception e){
				}
			}while(true);
			
			retailerCount--;
			System.out.println("Ending Retailer..." + rNumber);
		}
		
		private boolean hasFile(File directory, String fileName){
			int count = directory.list(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return name.equalsIgnoreCase(fileName);
				}
			}).length;
			
			if( count > 0 )
				return true;
			
			File[] folders = directory.listFiles(new FileFilter() {
				
				@Override
				public boolean accept(File f) {
					return f.isDirectory();
				}
			});
			
			boolean hasFile;
			
			for(File f : folders){
				hasFile = hasFile(f, fileName);
				if( hasFile )
					return true;
			}
			
			return false;
		}
		
		private List<File> filterForPythonFiles(File directory){
			List<File> files = new ArrayList<>();
			
			File[] selectedFiles = directory.listFiles(new FileFilter() {
				@Override
				public boolean accept(File file) {
					if( file.isDirectory() || file.getName().trim().toLowerCase().endsWith(".py") )
						return true;
					return false;
				}
			});
			
			for(File file : selectedFiles){
				if( file.isDirectory() )
					files.addAll(filterForPythonFiles(file));
				else
					files.add(file);
			}
			
			return files;
		}
		
		private Set<String> extractDependencies(File directory){
			Set<String> dependencies = new HashSet<>();
			List<File> files = filterForPythonFiles(directory);
			
			String line, temp;
			String[] parts, parts2;
			for(File file : files){
				try( BufferedReader reader = new BufferedReader(new FileReader(file)) ){
					while( (line = reader.readLine()) != null ){
						line = line.trim().toLowerCase();
						
						try{
							if( line.startsWith("import ") || line.startsWith("from ") 
									|| line.startsWith("import\t") || line.startsWith("from\t") ){
								if( line.startsWith("import") ){
									temp = line.substring("import".length() + 1);
									parts = temp.trim().split(",");
									
									for( String s : parts ){
										if( s.trim().isEmpty() )
											continue;
										
										parts2 = s.split("\\s+");
										
										if( parts2[0].trim().isEmpty() )
											continue;
										
										if( parts2[0].contains(".") )
											dependencies.add(parts2[0].substring(0, parts2[0].indexOf(".")).trim());
										else
											dependencies.add(parts2[0].trim());
									}
								}
								else{
									parts = line.split("\\s+");
									
									if( parts[1].trim().isEmpty() )
										continue;
									
									if( parts[1].contains(".") )
										dependencies.add(parts[1].substring(0, parts[1].indexOf(".")).trim());
									else
										dependencies.add(parts[1].trim());
								}
							}
						}
						catch(Exception e){
							System.err.println("Import line is: " + line);
						}
					}
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			
			return dependencies;
		}
	}
	
	
	private Integer cCount = 0;
	
	private class Consumer implements Runnable{
		private int runTimes;
		private Query query, q;
		private int cNumber;
		
		public Consumer(){
			query = new Query("INSERT INTO github_final_projects VALUES (?, ?, ?, ?, ?, ?, ?, ?)", Util.mcon);
			if( !query.isFullyConnected() ){
				query.getException().printStackTrace();
				query = null;
				return;
			}
			
			q = new Query("INSERT INTO project_dependencies VALUES (NULL, ?, ?)", Util.mcon);
			if( !q.isFullyConnected() ){
				q.getException().printStackTrace();
				q = null;
				return;
			}
			
			synchronized(cCount){
				if( cCount < Integer.MAX_VALUE )
					cNumber = cCount++;
				else
					cNumber = cCount = 2;
			}
		}
		
		public Consumer(int runTimes){
			this();
			this.runTimes = runTimes;
		}
		
		public void run(){
			System.out.println("Starting Consumer..." + cNumber);
			
			while( !producerStarted ){
				try{
					Thread.sleep(100);
				}catch(Exception e){}
			}
			
			do{
				try{
					SimpleMap<Project, Set<String>> projectDependencyMap = consumerQueue.poll();
					
					while( projectDependencyMap == null && retailerCount > 0 ){
						if( runTimes == 1 )
							break;
						
						//check if there is work to be done elsewhere
						//wholesaler first, then retailer
//						if( (!wholeSalerQueue.isEmpty() && wholeSalerQueue.remainingCapacity() > 0) || retailerQueue.isEmpty() )
//							new WholeSaler(1).run();
//						else if( (!retailerQueue.isEmpty() && retailerQueue.remainingCapacity() > 0) || consumerQueue.isEmpty() )
//							new Retailer(1).run();
						
						
						if( (!retailerQueue.isEmpty() && retailerQueue.remainingCapacity() >= 0 && retailerQueue.remainingCapacity() < 4) || consumerQueue.isEmpty() )
							new Retailer(1).run();
						else if( (!wholeSalerQueue.isEmpty() && wholeSalerQueue.remainingCapacity() >= 0 && wholeSalerQueue.remainingCapacity() < 5) || retailerQueue.isEmpty() )
							new WholeSaler(1).run();
						
						
						projectDependencyMap = consumerQueue.poll();
					}
					
					if( projectDependencyMap == null )
						break;
					
					System.out.println("Consumer " + cNumber + " Checking Dependencies...");
					
					if( checkExtractedDependencies(projectDependencyMap.getSecondElement(), projectDependencyMap.getFirstElement()) ){
						saveProject(projectDependencyMap.getFirstElement());
						saveDependencies(projectDependencyMap.getSecondElement(), projectDependencyMap.getFirstElement());
					}
					
					System.out.println("Consumer " + cNumber + " Finished Checking Dependencies...");
					
					if( runTimes == 1 )
						break;
				}
				catch(Exception e){}
			}while(true);
			
			System.out.println("Leaving Consumer..." + cNumber);
			
			query.disconnect();
			q.disconnect();
		}
		
		private boolean checkExtractedDependencies(Set<String> dependencies, Project project){
			System.out.println("Consumer " + cNumber + " is checking " + dependencies.size() + " imports...");
			ServerPostingManager spm = new ServerPostingManager(null);
			
			for(String dependency : dependencies){
				if( dependency.trim().isEmpty() )
					continue;
				if( !spm.isURLLinkAlive(buildPyPIRepoURL(dependency)) ){
					//save for writing to the failed dependency CSV
					try{
						dependenciesWriter.write(project.getId() + "," + project.getUrl() + "," + dependency);
						dependenciesWriter.newLine();
					}catch(Exception e){}
					
					return false;
				}
			}
			
			return true;
		}
		
		private String buildPyPIRepoURL(String dependency){
			return "https://pypi.python.org/pypi/" + dependency + "/json";
		}
		
		private void saveProject(Project project){
			try{
				PreparedStatement ps = query.getPS();
				
				ps.setInt(1, project.getId());
				ps.setString(2, project.getUrl());
				ps.setBoolean(3, project.isDeleted());
				ps.setString(4, project.getCreatedAt());
				ps.setString(5, project.getUpdatedAt());
				ps.setInt(6, project.getForkedFrom());
				ps.setBoolean(7, (Boolean) project.getMetaData("hasSetupFile"));
				ps.setBoolean(8, (Boolean) project.getMetaData("hasRequirementsFile"));
				
				ps.executeUpdate();
			}
			catch(Exception e){
				//e.printStackTrace();
			}
		}
		
		private void saveDependencies(Set<String> dependencies, Project project){
			try{
				PreparedStatement ps = q.getPS();
				
				int count = 0;
				
				for(String dependency : dependencies){
					if( dependency.trim().isEmpty() )
						continue;
					
					ps.setInt(1, project.getId());
					ps.setString(2, dependency);
					
					ps.addBatch();
					count++;
				}
				
				if( count > 0 )
					ps.executeBatch();
			}
			catch(Exception e){
				//e.printStackTrace();
			}
		}
	}
}
