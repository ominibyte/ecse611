package ca.mcgill.ecse611.group2.replication;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import ca.mcgill.ecse611.Query;
import ca.mcgill.ecse611.Util;

public class GithubOnlyProjectsFilterer2 {
	private LinkedList<String> pyPiProjects;
	private static final String PYPI_PROJECTS_CSV = "C:\\Users\\Richboy\\Documents\\ECSE611\\finished data\\project_url_map.csv";
	private ExecutorService executor;
	private ArrayBlockingQueue<Project> queue;
	private boolean producerFinished;
	
	public GithubOnlyProjectsFilterer2(){
		pyPiProjects = new LinkedList<>();
		executor = Executors.newFixedThreadPool(6);
		queue = new ArrayBlockingQueue<>(10);
		
		loadPyPiProjects();
		filterForGithubOnlyProjects();
		printGithubOnlyProjectsCount();
		
		executor.shutdown();
		System.out.println("\n\nDone!!!");
	}
	
	private void loadPyPiProjects(){
		File file = new File(PYPI_PROJECTS_CSV);
		
		int row = 0;
		String line;
		String[] parts;
		
		try(BufferedReader reader = new BufferedReader(new FileReader(file))){
			while( (line = reader.readLine()) != null ){
				row++;
				if( row == 1 )
					continue;
				
				parts = line.split(",");
				
				pyPiProjects.add("https://api.github.com/repos/" + parts[1].substring(parts[1].indexOf("com/") + "com/".length()));
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void filterForGithubOnlyProjects(){
		int count = 0;
		
		//get the total number of projects in the database
		Query q = new Query("SELECT COUNT(*) AS cnt FROM python_projects", Util.mcon);
		if( q.isFullyConnected() ){
			try{
				ResultSet rs = q.getPS().executeQuery();
				rs.next();
				count = rs.getInt("cnt");
				rs.close();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		else{
			q.disconnect();
			q.getException().printStackTrace();
			return;
		}
		q.disconnect();
		
		System.out.println("Total Count is " + count);
		
		Future<String>[] futures = new Future[5];
		
		executor.execute(new Producer());
		
		for(int i = 0; i < futures.length; i++){
			futures[i] = executor.submit(new Consumer(), "");
		}
		
		for(int i = 0; i < futures.length; i++){
			try {
				futures[i].get();
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void printGithubOnlyProjectsCount(){
		//get the total number of projects in the database
		Query q = new Query("SELECT COUNT(*) AS cnt FROM github_only_projects", Util.mcon);
		if( q.isFullyConnected() ){
			try{
				ResultSet rs = q.getPS().executeQuery();
				rs.next();
				System.out.println(rs.getInt("cnt") + " Projects exists in Github only!");
				rs.close();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		else{
			q.disconnect();
			q.getException().printStackTrace();
			return;
		}
		q.disconnect();
	}
	
	public static void main(String[] args){
		new GithubOnlyProjectsFilterer2();
	}
	
	public class Producer implements Runnable{
		public void run(){
			Query q = new Query("SELECT * FROM python_projects", Util.mcon);
			if( q.isFullyConnected() ){
				try{
					ResultSet rs = q.getPS().executeQuery();
					while(rs.next()){
						Project p = new Project(rs.getInt("id"), rs.getString("url"), rs.getBoolean("deleted"), 
								rs.getString("created_at"), rs.getString("created_at"), rs.getInt("forked_from"));
						
						queue.put(p);
					}
					rs.close();
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			else{
				q.getException().printStackTrace();
			}
			q.disconnect();
			
			producerFinished = true;
		}
	}
	
	private class Consumer implements Runnable{
		private Query query;
		
		public Consumer(){
			query = new Query("INSERT INTO github_only_projects VALUES (?, ?, ?, ?, ?, ?)", Util.mcon);
			if( !query.isFullyConnected() ){
				query.getException().printStackTrace();
				query = null;
				return;
			}
		}
		
		public void run(){
			do{
				Project p = queue.poll();
				
				while( p == null && !producerFinished ){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					p = queue.poll();
				}
				
				if( p == null )
					break;
				
				if( !pyPiProjects.contains(p.getUrl()) )
					saveProject(p);
			}while(true);
			
			if( query != null )
				query.disconnect();
		}
		
		private void saveProject(Project project){
			try{
				PreparedStatement ps = query.getPS();
				
				ps.setInt(1, project.getId());
				ps.setString(2, project.getUrl());
				ps.setBoolean(3, project.isDeleted());
				ps.setString(4, project.getCreatedAt());
				ps.setString(5, project.getUpdatedAt());
				ps.setInt(6, project.getForkedFrom());
				
				ps.executeUpdate();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}
}
