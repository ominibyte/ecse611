package ca.mcgill.ecse611.group2.replication;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import org.json.me.JSONObject;

public class Graph10DataGenerator {
	private Map<String, Integer> versionParts;
	private static final String JSON_FILES_FOLDER = "/Users/Richboy/Documents/ECSE611/Replication/json_files";
	private static final String CSV_FILE = "/Users/Richboy/Documents/ECSE611/Replication/graph10_data.csv";
	private BufferedWriter writer;
	
	public Graph10DataGenerator(){
		init();
		process();
		cleanup();
	}
	
	private void init(){
		try {
			writer = new BufferedWriter(new FileWriter(new File(CSV_FILE)));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		versionParts = new HashMap<>();
		
		for(int i = 0; i < 10; i++){
			versionParts.put("major" + i, 0);
			versionParts.put("minor" + i, 0);
			versionParts.put("patch" + i, 0);
		}
		
		try{
			writer.write("version, major, minor, patch");
			writer.newLine();
		}catch(Exception e){}
	}
	
	private void process(){
		File folder = new File(JSON_FILES_FOLDER);
		
		File[] files = folder.listFiles(new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().trim().endsWith(".json");
			}
		});
		
		JSONObject obj, releases;
		Enumeration<String> versions;
		String version;
		String[] parts;
		int major, minor, patch;
		int count = 0;
		int total = files.length;
		
		System.out.println(total + " files found!\n");
		
		for(File file : files){
			count++;
			
			System.out.printf("\r%d of %d (%d%s)", count, total, count * 100 / total, "%");
			
			try{
				obj = new JSONObject(new String(Files.readAllBytes(file.toPath())));
				releases = obj.getJSONObject("releases");
				versions = releases.keys();
				
				while( versions.hasMoreElements() ){
					version = regularizeVersion(versions.nextElement());
					parts = version.split("\\.");
					
					try{
						major = Integer.parseInt(parts[0]);
						minor = parts.length > 1 ? Integer.parseInt(parts[1]) : 0;
						patch = parts.length > 2 ? Integer.parseInt(parts[2]) : 0;
						
						if( major < 10 && minor < 10 && patch < 10 ){
							versionParts.put("major" + major, versionParts.get("major" + major) + 1);
							versionParts.put("minor" + minor, versionParts.get("minor" + minor) + 1);
							versionParts.put("patch" + patch, versionParts.get("patch" + patch) + 1);
						}
					}catch(Exception e){}
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	private void cleanup(){
		for(int i = 0; i < 10; i++){
			try{
				writer.write(i + "");
				writer.write("," + versionParts.get("major" + i));
				writer.write("," + versionParts.get("minor" + i));
				writer.write("," + versionParts.get("patch" + i));
				writer.newLine();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		
		try {
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("\n\nDONE!!!");
	}
	
	private String regularizeVersion(String version){
		version = version.replaceAll("[a-zA-Z]", ".");
		
		while( version.contains("..") )
			version = version.replace("..", ".");
		
		while( version.split("\\.").length < 3 )
			version += ".0";
		
		while( version.contains("..") )
			version = version.replace("..", ".");
		
		try{
			String[] parts = version.split("\\.");
			
			version = Integer.parseInt(parts[0]) + "." + Integer.parseInt(parts[1]) + "." + Integer.parseInt(parts[2]);
		}catch(Exception e){}
		
		return version;
	}
	
	
	public static void main(String[] args){
		new Graph10DataGenerator();
	}
}
