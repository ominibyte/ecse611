package ca.mcgill.ecse611.group2.replication;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.mcgill.ecse611.Util.SimpleMap;

public class GitHubRankMerger {
	private static final String RANKS_PATH = "/Users/Richboy/Documents/ECSE611/Replication/RankFiles";
	private static final String COLLATED_RANKS_FILE = "/Users/Richboy/Documents/ECSE611/Replication/collated_github_dependency_ranks.csv";
	private Map<String, Integer> dependencyCountMap;
	private List<SimpleMap<String, Integer>> dependencyRanks;
	private List<String> sysLibs;
	private static final String SYS_LIB = "/Users/Richboy/Documents/ECSE611/Replication/RankFiles/libList.txt";
	
	public GitHubRankMerger(){
		dependencyCountMap = new HashMap<>();
		dependencyRanks = new ArrayList<>();
		sysLibs = new ArrayList<>();
		
		loadSystemLibraries();
		gatherDependencies();
		orderDependencies();
		printDependencyRanks();
		
		System.out.println("\n\nDONE!!!");
	}
	
	private void loadSystemLibraries(){
		try( BufferedReader reader = new BufferedReader(new FileReader(new File(SYS_LIB))) ){
			String line;
			
			while( (line = reader.readLine()) != null ){
				if( !line.trim().isEmpty() )
					sysLibs.add(line.trim().toLowerCase());
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void gatherDependencies(){
		//find all csv files
		File[] files = new File(RANKS_PATH).listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().trim().endsWith(".csv");
			}
		});
		
		String line;
		String[] parts, parts2;
		int count;
		
		for( File file : files ){
			try( BufferedReader reader = new BufferedReader(new FileReader(file)) ){
				while( (line = reader.readLine()) != null ){
					parts = line.split("\\,");
					parts2 = parts[0].split("\\s+");
					try{
						count = Integer.parseInt(parts[parts.length - 1]);
					}catch(Exception e){
						continue;
					}
					
					dependencyCountMap.merge(parts2[0].toLowerCase(), count, (oldValue, newValue) -> oldValue + newValue);
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	private void orderDependencies(){
		dependencyCountMap.forEach((key, value) -> dependencyRanks.add(new SimpleMap<>(key, value)));
		
		dependencyCountMap.clear();
		
		Collections.sort(dependencyRanks, (e1, e2) -> {
			if( e1.getSecondElement() > e2.getSecondElement() )
				return -1;
			else if( e2.getSecondElement() > e1.getSecondElement() )
				return 1;
			return 0;
		});
	}
	
	private void printDependencyRanks(){
		try( BufferedWriter writer = new BufferedWriter(new FileWriter(new File(COLLATED_RANKS_FILE))) ){
			writer.write("Package,Dependency Count,Dependency Rank");
			writer.newLine();
			int rank = 1;
			
			for(SimpleMap<String, Integer> pair : dependencyRanks){
				if( sysLibs.contains(pair.getFirstElement()) )
					continue;
				writer.write(pair.getFirstElement() + "," + pair.getSecondElement() + "," + rank++);
				writer.newLine();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args){
		new GitHubRankMerger();
	}
}
