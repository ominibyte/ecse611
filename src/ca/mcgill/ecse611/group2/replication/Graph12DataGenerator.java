package ca.mcgill.ecse611.group2.replication;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.json.me.JSONArray;
import org.json.me.JSONObject;

import ca.mcgill.ecse611.Query;
import ca.mcgill.ecse611.ServerPostingInterface;
import ca.mcgill.ecse611.ServerPostingManager;
import ca.mcgill.ecse611.Util;
import ca.mcgill.ecse611.Util.ErrorContext;
import ca.mcgill.ecse611.Util.SimpleMap;


public class Graph12DataGenerator {
	private static final String REFERENCE_PACKAGE = "requests";//django
	private List<Project> dependentProjects;
	public static final String PYPI_PYPI_URL = "https://pypi.python.org/pypi/";
	private LinkedList<SimpleMap<String, String>> packageReleases;//version,upload time
	//Integer maps to the index in packageReleases, the set to total projects found, the map to the last change for the package
	private Map<Integer, SimpleMap<Set<Integer>, Map<Integer, String>>> packageAdoptionMap;
	private static final String[] accessTokens = {
		"b71e9f8d063a1c4eb8d0b765155835e68bdbdcae",//richboy
		"d652a0345a2206ab7445ed8aa1ba116e7f06b0ef",//richard
		"22895ed193309b1b633788825b4a4d33ca093a29",//aprajita
		"b5f2adc9aa015bf35a8830541d4c070cbc2a8f4b",//sahil
		"707e84b92c99e6130f1ce9bb644a4670f02dcd39"//ruchika
	};
	private Integer tokenIndex = 0;
	private Integer packageReleasesIndex = 0;
	private ExecutorService executor;
	private static final int THREAD_COUNT = 4;
	private static final String MAJOR_OUTPUT_FILE = "/Users/Richboy/Documents/ECSE611/Replication/graph12_major_data.csv";
	private static final String MINOR_OUTPUT_FILE = "/Users/Richboy/Documents/ECSE611/Replication/graph12_major_data.csv";
	private static final String PATCH_OUTPUT_FILE = "/Users/Richboy/Documents/ECSE611/Replication/graph12_major_data.csv";
	private int totalWithOtherVersion;
	
	public Graph12DataGenerator(){
		executor = Executors.newFixedThreadPool(THREAD_COUNT);
		dependentProjects = new ArrayList<>();
		packageReleases = new LinkedList<>();
		packageAdoptionMap = new LinkedHashMap<>();//the total number of projects resolving to a particular version
		
		//get all projects depending on the reference package
		//fetchDependentProjects();
		
		//check the reference package's metadata for all its releases and the date it was released
		fetchPackageReleases();
		System.out.println(packageReleases.size() + " Package Releases Found!");
		
		//get the commit history of all the projects
		processProjectCommits();
		
		executor.shutdown();
		
		Set<Integer> totalFound = new HashSet<>();
		Map<Integer, String> currentState = new HashMap<>();//this holds the current package version state
		LinkedList<SimpleMap<Integer, Double>> percentageResolved = new LinkedList<>();//this holds the index to the packageReleases and the percentage that resolved to *
		
		
		//we need to cummulate all the data
		packageAdoptionMap.forEach((i, map) -> {
			map.getFirstElement().forEach(totalFound::add);
			map.getSecondElement().forEach( (projectID, state) -> {
				currentState.put(projectID, state);
			} );
			
			totalWithOtherVersion = 0;
			//calculate the total state that is not *
			currentState.forEach((projectID, state) -> {
				if( !state.equals("*") )//TODO this check may not be right. A version with 2.x.x or >2 may work for * as well
					totalWithOtherVersion++;
			});
			
			percentageResolved.add( new SimpleMap<>(i, (double) ((totalFound.size() - totalWithOtherVersion) * 100 / totalFound.size())) );
		});
		
		String regularizedVersion;
		//final CSV file should have
		//version,release date,percent (fraction of applications that resolved)
		try( BufferedWriter writer = new BufferedWriter(new FileWriter(new File(MAJOR_OUTPUT_FILE))) ){
			writer.write("version,regularized version,release date,percent resolved,version weight,release timestamp");
			
			for( SimpleMap<Integer, Double> entry : percentageResolved ){
				if( isMajorVersion(regularizedVersion = regularizeVersion(packageReleases.get(entry.getFirstElement()).getFirstElement())) ){
					writer.write(packageReleases.get(entry.getFirstElement()).getFirstElement() + "," + regularizedVersion + "," +
							packageReleases.get(entry.getFirstElement()).getSecondElement() + "," + entry.getSecondElement() + "," +
							calculateVersionWeight(regularizedVersion) + "," + getReleaseTimestamp(packageReleases.get(entry.getFirstElement()).getSecondElement()));
					writer.newLine();
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		try( BufferedWriter writer = new BufferedWriter(new FileWriter(new File(MINOR_OUTPUT_FILE))) ){
			writer.write("version,regularized version,release date,percent resolved,version weight,release timestamp");
			
			for( SimpleMap<Integer, Double> entry : percentageResolved ){
				if( isMinorVersion(regularizedVersion = regularizeVersion(packageReleases.get(entry.getFirstElement()).getFirstElement())) ){
					writer.write(packageReleases.get(entry.getFirstElement()).getFirstElement() + "," + regularizedVersion + "," +
							packageReleases.get(entry.getFirstElement()).getSecondElement() + "," + entry.getSecondElement() + "," +
							calculateVersionWeight(regularizedVersion) + "," + getReleaseTimestamp(packageReleases.get(entry.getFirstElement()).getSecondElement()));
					writer.newLine();
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		try( BufferedWriter writer = new BufferedWriter(new FileWriter(new File(PATCH_OUTPUT_FILE))) ){
			writer.write("version,regularized version,release date,percent resolved,version weight,release timestamp");
			
			for( SimpleMap<Integer, Double> entry : percentageResolved ){
				if( isPatchVersion(regularizedVersion = regularizeVersion(packageReleases.get(entry.getFirstElement()).getFirstElement())) ){
					writer.write(packageReleases.get(entry.getFirstElement()).getFirstElement() + "," + regularizedVersion + "," +
							packageReleases.get(entry.getFirstElement()).getSecondElement() + "," + entry.getSecondElement() + "," +
							calculateVersionWeight(regularizedVersion) + "," + getReleaseTimestamp(packageReleases.get(entry.getFirstElement()).getSecondElement()));
					writer.newLine();
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		System.out.println("\n\nDONE!!!");
	}
	
	private void fetchPackageReleases(){
		ServerPostingManager spm = new ServerPostingManager(new ServerPostingInterface() {
			@Override
			public void retriveMessage(ErrorContext context) {
				try{
					JSONObject obj = new JSONObject(context.getMessage());
					JSONObject releases = obj.getJSONObject("releases");
					Enumeration<String> versions = releases.keys();
					String version;
					int added = 0;
					
					while( versions.hasMoreElements() ){
						try{
							version = versions.nextElement();
							
							packageReleases.add(new SimpleMap<>(version, releases.getJSONArray(version).getJSONObject(0).getString("upload_time")));
							added++;
							if( added == 10 )
								break;
						}
						catch(Exception e){
						}
					}
					
					//sort releases in ascending order by upload time
					Collections.sort(packageReleases, (r1, r2) -> {
						try{
							if( isAnEarlierRelease(r1, r2) )
								return -1;
							else if( isAnEarlierRelease(r2, r1) )
								return 1;
							return 0;
						}
						catch(Exception e){
							e.printStackTrace();
							return 0;
						}
					});
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			
			@Override
			public void interruptPosting(String text) {
				System.err.println(text);
			}
		});
		spm.setTargetURL(PYPI_PYPI_URL + REFERENCE_PACKAGE + "/json");
		spm.setAsGetRequest();
		spm.setURLParameters("");
		spm.run();
	}
	
	private void fetchDependentProjects(){
		Query q = new Query("SELECT * FROM github_final_projects INNER JOIN project_dependencies ON github_final_projects.id = project_dependencies.fkproject WHERE deleted = 0 AND dependency = '"+ REFERENCE_PACKAGE +"'", Util.mcon);
		if( q.isFullyConnected() ){
			try{
				ResultSet rs = q.getPS().executeQuery();
				Project p = new Project(rs.getInt("id"), rs.getString("url"), rs.getBoolean("deleted"), 
						rs.getString("created_at"), rs.getString("created_at"), rs.getInt("forked_from"));
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		q.disconnect();
	}
	
	private void processProjectCommits(){
		Future<String>[] futures = new Future[THREAD_COUNT];
		for( int i = 0; i < futures.length; i++){
			futures[i] = executor.submit(new VersionChecker(), "");
		}
		
		for( int i = 0; i < futures.length; i++){
			try{
				futures[i].get();
			}catch(Exception e){}
		}
	}
	
	private String regularizeVersion(String v){
		String version = v + "";
		version = version.replaceAll("[a-zA-Z]", ".");
		
		while( version.contains("..") )
			version = version.replace("..", ".");
		
		while( version.split("\\.").length < 3 )
			version += ".0";
		
		while( version.contains("..") )
			version = version.replace("..", ".");
		
		return version;
	}
	
	private boolean isSmaller(String version1, String version2) throws Exception{
		String[] parts1 = version1.split("\\.");
		String[] parts2 = version2.split("\\.");
		
		int major1 = Integer.parseInt(parts1[0]);
		int minor1 = parts1.length > 1 ? Integer.parseInt(parts1[1]) : 0;
		int patch1 = parts1.length > 2 ? Integer.parseInt(parts1[2]) : 0;
		
		int major2 = Integer.parseInt(parts2[0]);
		int minor2 = parts2.length > 1 ? Integer.parseInt(parts2[1]) : 0;
		int patch2 = parts2.length > 2 ? Integer.parseInt(parts2[2]) : 0;
		
		if( major1 < major2 )
			return true;
		if( major1 == major2 && minor1 < minor2 )
			return true;
		if( major1 == major2 && minor1 == minor2 && patch1 < patch2 )
			return true;
		
		return false;
	}
	
	private boolean isAnEarlierRelease(SimpleMap<String, String> release1, SimpleMap<String, String> release2) throws Exception{
		//2014-12-28T00:54:02
		long time1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(release1.getSecondElement()).getTime();
		long time2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(release2.getSecondElement()).getTime();
		
		return time1 < time2;
	}
	
	private long getReleaseTimestamp(String releaseTime){
		try{
			//2014-12-28T00:54:02
			return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(releaseTime).getTime();
		}
		catch(Exception e){
			return 0;
		}
	}
	
	private boolean isPatchVersion(String version){
		String[] parts = version.split("\\.");
		
		return (parts[2].trim().length() == 1 && !version.endsWith("0")) || (parts[2].trim().length() > 1);
	}
	
	private boolean isMinorVersion(String version){
		String[] parts = version.split("\\.");
		
		return !isPatchVersion(version) && 
				((parts[1].trim().length() == 1 && !parts[1].endsWith("0")) || (parts[1].trim().length() > 1));
	}
	
	private boolean isMajorVersion(String version){
		return !isPatchVersion(version) && !isMinorVersion(version);
	}
	
	private double calculateVersionWeight(String version){
		String[] parts = version.split("\\.");
		
		return Integer.parseInt(parts[0]) + function(Integer.parseInt(parts[1]) + function(Integer.parseInt(parts[2])));
	}
	
	private double function(double number){
		return 1 - (1 / (0.5 * number + 1));
	}
	
	public static void main(String[] args){
		new Graph12DataGenerator();
	}
	
	private class VersionChecker implements Runnable{
		private Set<Integer> totalProjectsFound;//all the projects encountered here
		//Integer, the project ID, String the last version seen (* or #)
		private Map<Integer, String> projectsLastVersionState;//the last change state of the project
		private String requestsLine;
		private int currentProjectID;
		
		public VersionChecker(){
			totalProjectsFound = new LinkedHashSet<>();
			projectsLastVersionState = new LinkedHashMap<>();
		}
		
		public void run(){
			while( true ){
				totalProjectsFound = new LinkedHashSet<>();
				projectsLastVersionState = new LinkedHashMap<>();
				
				SimpleMap<String, String> releaseTimePair = null;
				SimpleMap<String, String> prevReleaseTimePair = null;
				int index = -1;
				
				synchronized(packageReleasesIndex){
					index = packageReleasesIndex++;
				}
				
				if( index >= packageReleases.size() )
					return;
				
				System.out.println("Processing " + (index + 1) + " of " + packageReleases.size());
				
				releaseTimePair = packageReleases.get(index);
				if( index > 0 )
					prevReleaseTimePair = packageReleases.get(index - 1);
				
				//TODO we could do this by projects. So get one project at a time, check if we can access it, get all the commits and process
				//get all commits between the release time
				String queryString = "SELECT * FROM projects_using_requests_commits WHERE ";
				
				if( index == 0 )
					queryString += "created_at < '" + releaseTimePair.getSecondElement() + "'";
				else
					queryString += "created_at > '" + prevReleaseTimePair.getSecondElement() + "' AND created_at < '" + releaseTimePair.getSecondElement() + "'";
				
				queryString += " ORDER BY created_at ASC";
				
				Query q = new Query(queryString, Util.mcon);
				if( q.isFullyConnected() ){
					try{
						ResultSet rs = q.getPS().executeQuery();
						
						while(rs.next()){
							Commit c = new Commit(rs.getLong("id"), rs.getString("sha"), rs.getInt("author_id"), 
									rs.getInt("commiter_id"), rs.getInt("project_id"), rs.getString("created_at"));
							
							processCommit(c);
						}
						
						rs.close();
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
				else
					q.getException().printStackTrace();
				q.disconnect();
				
				packageAdoptionMap.put(index, new SimpleMap<>(totalProjectsFound, projectsLastVersionState));
			}
		}
		
		private void processCommit(Commit c){
			//get the JSON file for this commit
			Project p = getProject(c.getProjectID());
			if( p == null )
				return;
			
			//add to total projects found
			totalProjectsFound.add(p.getId());
			currentProjectID = p.getId();
			
			String url = getCommitJSONURL(p.getUrl(), c.getSha());
			
			ServerPostingManager spm = new ServerPostingManager(new ServerPostingInterface() {
				
				@Override
				public void retriveMessage(ErrorContext context) {
					if( context.isSuccessful() )
						processResponse(context.getMessage());
					else
						System.err.println(context.getMessage());
				}
				
				@Override
				public void interruptPosting(String text) {
					System.err.println(text);
				}
			});
			
			spm.setRetriesCount(3);
			spm.setAsGetRequest();
			spm.setURLParameters("");
			spm.setTargetURL(url);
			spm.run();
		}
		
		private void processResponse(String responseText){
			try{
				JSONObject obj = new JSONObject(responseText);
				JSONArray files = obj.getJSONArray("files");
				JSONObject file;
				
				for(int i = 0; i < files.length(); i++){
					file = files.getJSONObject(i);
					
					if( meetsSpecs(file.getString("filename")) ){
						String requestsLine = getRequestsLine(file.getString("filename"), file.getString("raw_url"));
						if( requestsLine != null )
							addRequests(requestsLine);
					}
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		
		private boolean meetsSpecs(String fileName){
			if( fileName.toLowerCase().trim().equals("setup.py") )
				return true;
			if( fileName.toLowerCase().trim().equals("requirements.txt") )
				return true;
			if( fileName.toLowerCase().trim().equals("constraints.txt") )
				return true;
			
			return false;
		}
		
		private boolean isSetupDotPy(String fileName){
			if( fileName.toLowerCase().trim().equals("setup.py") )
				return true;
			
			return false;
		}
		
		private String getRequestsLine(String fileName, String rawURL){
			requestsLine = null;
			
			ServerPostingManager spm = new ServerPostingManager(new ServerPostingInterface() {
				
				@Override
				public void retriveMessage(ErrorContext context) {
					if( !context.isSuccessful() )
						return;
					
					//TODO could move this to another method
					if( isSetupDotPy(fileName) ){
						//search for install_requires
						String block = null;
						String newMessage = context.getMessage().replaceAll("#.+", "").replaceAll("\"", "'").replaceAll("\\%\\([^\\(]+\\)", "").replaceAll("\\s", "").replaceAll("\\,\\]", "]");
						if( newMessage.contains("install_requires=[]") )
							return;
						else if( newMessage.contains("install_requires=[") ){
							int index = newMessage.indexOf("install_requires=[");
							index = index + "install_requires=[".length() - 1;
							int nextIndex = newMessage.indexOf("']", index);
							
							try{
								block = newMessage.substring(index, nextIndex + "']".length());
							}
							catch(Exception e){
								System.err.println(newMessage.substring(newMessage.indexOf("install_requires=[")));
								return;
							}
						}
						//then it may link to a variable, lets get the requirements in the variable
						else if( newMessage.contains("install_requires=") && newMessage.indexOf("install_requires=", newMessage.indexOf("setup(")) > 0 ){
							int index = newMessage.indexOf("install_requires=", newMessage.indexOf("setup("));
							int v;
							int nextIndex = Math.min((v = newMessage.indexOf(")", index)) == -1 ? 10000 : v, (v = newMessage.indexOf(",", index)) == -1 ? 10000 : v);
							
							String variable = newMessage.substring(index + "install_requires=".length(), nextIndex);
							
							if( newMessage.contains(variable + "=[") ){
								index = newMessage.indexOf(variable + "=[");
								nextIndex = newMessage.indexOf("']", index);
								block = newMessage.substring(index - 1, nextIndex + "']".length());
							}
							else{
								System.err.println("Cannot find variable: " + variable);
								System.err.println(newMessage);
								return;
							}
						}
						else
							return;
						
						try{
							String[] parts = block.trim().split("'\\,'");
							
							for( String part : parts ){
								part = part.replaceAll("'", "");
								if( part.startsWith("[") )
									part = part.substring(1);
								if( part.endsWith("]") )
									part = part.substring(0, part.length() - 1);
								
								if( part.trim().toLowerCase().startsWith(REFERENCE_PACKAGE) ){
									requestsLine = part.trim().toLowerCase();
									break;
								}
							}
						}
						catch(Exception e){
							System.out.println("Block: " + block);
							System.err.println(context.getMessage());
						}
					}
					else{//requirements or constraints
						String[] parts = context.getMessage().split("\n");
						for(String line : parts){
							if( line.trim().toLowerCase().startsWith(REFERENCE_PACKAGE) ){
								requestsLine = line.trim().toLowerCase();
								break;
							}
						}
					}
				}
				
				@Override
				public void interruptPosting(String text) {
					System.err.println(text);
				}
			});
			
			spm.setRetriesCount(3);
			spm.setAsGetRequest();
			spm.setURLParameters("");
			spm.setTargetURL(rawURL);
			spm.run();
			
			return requestsLine;
		}
		
		private void addRequests(String requestLine){
			//request line may have several
			if( requestLine.equalsIgnoreCase(REFERENCE_PACKAGE) )
				projectsLastVersionState.put(currentProjectID, "*");
			else
				projectsLastVersionState.put(currentProjectID, requestLine.substring(requestLine.length()));
		}
		
		private String getCommitJSONURL(String apiURL, String sha){
			return apiURL + "/commits/" + sha + "?access_token=" + accessTokens[getTokenIndex()];
		}
		
		//get the access token to use
		private int getTokenIndex(){
			int tIndex = -1;
			synchronized (tokenIndex) {
				tIndex = tokenIndex = (tokenIndex + 1) % accessTokens.length;
			}
			return tIndex;
		}
		
		private Project getProject(int projectID){
			Project p = null;
			
			Query q = new Query("SELECT * FROM requests_projects WHERE id = " + projectID, Util.mcon);
			if( q.isFullyConnected() ){
				try{
					ResultSet rs = q.getPS().executeQuery();
					rs.next();
					
					p = new Project(rs.getInt("id"), rs.getString("url"), rs.getBoolean("deleted"), 
							rs.getString("created_at"), rs.getString("created_at"), rs.getInt("forked_from"));
					p.addMetaData("hasSetupFile", rs.getInt("has_setup_file") > 0);
					p.addMetaData("hasRequirementsFile", rs.getInt("has_requirements_file") > 0);
					
					rs.close();
				}
				catch(Exception e){
					
				}
			}
			q.disconnect();
			
			return p;
		}
	}
}
